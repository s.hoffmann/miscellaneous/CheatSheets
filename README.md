# CheatSheets
**Collection of commands and hotkeys for various programs.**
### Usages:
- [Vim](https://github.com/derMacon/CheatSheets/blob/master/VimCommands.md)
- [IDEA Intellij](https://github.com/derMacon/CheatSheets/blob/master/IDEAHotkeys.md)
- [Ubuntu](https://github.com/derMacon/CheatSheets/blob/master/UbuntuCommands.md)
- [GDB (Ansi C Debugger)](https://github.com/derMacon/CheatSheets/blob/master/GDB_DebuggerGuide.md)
- [Subversion (VCS)](https://github.com/derMacon/CheatSheets/blob/master/SvnCommands.md)
- [Latex](https://github.com/derMacon/CheatSheets/blob/master/latexGuide/latexGuide.pdf)
